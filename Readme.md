
Web SDK is hosted by backend service, tcomm-proxy. 

Application developer is expected to, 

- Use the provided on-premise or cloud environment according to the instruction provided in [Web Setup](https://gitlab.com/livechatsdk/docs/-/wikis/web/Web-Setup) to get javascript sdk available to application.
And 
- Develop the application using javascript sdk documented at following URLs.
    - [Main](https://gitlab.com/livechatsdk/docs/-/wikis/web)
    - [Livechat API for session control]( https://gitlab.com/livechatsdk/docs/-/wikis/common/Livechat-API )
    - [Audio-Video API for calls]( https://gitlab.com/livechatsdk/docs/-/wikis/web/A-V-Functions )

[Integration environment](https://gitlab.com/livechatsdk/docs/-/wikis/%20env/Integration%20environment%20)

Script files in this project are for referece only and not updated frequently. Web application developer is NOT expected to download scripts from this project and use.
