import { Logger } from "./logger.js";

var Channel = function (request) {

    var channel = request;


    var prepareWSUrl = function () {
        var endpointPath = "/ws-chat-proxy/ws";
        var requestUrl = getRootUri() + endpointPath + channel;
        return requestUrl;
    }

    function getRootUri() {
        var uri = (document.location.protocol == "http" ? "ws://" : "wss://") + (document.location.hostname == "" ? "localhost" : document.location.hostname) + ":" +
            (document.location.port == "" ? "8080" : "8001");

        return uri;
    }



    return {
        messageHandler: function () {
            Logger.info('url:' + prepareWSUrl());
            websocket = new WebSocket("wss://localhost:8081/ws-chat-proxy/ws/chat");

            websocket.onopen = function (evt) {
                onOpen(evt);
            };

            websocket.onmessage = function (evt) {
                onMessage(evt);
            };
            websocket.onerror = function (evt) {
                onError(evt);
            };
            websocket.onclose = function (evt) {
                onClose(evt)
            };

            websocket.addEventListener('close', function (evt) {
                alert('connection lost');
            });
        }

    }
}

export default Channel;