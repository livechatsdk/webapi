# DESCRIPTION
The code pertaining to the js library Bowser is taken from this commit:
https://github.com/lancedikson/bowser/tree/f09411489ced05811c91cc6670a8e4ca9cbe39a7

Certain methods have been removed as they are not used. This is done to reduce file size after compilation.
If updating make sure the methods relevant are added.

# LICENSE
License info for this library can be found here:
https://github.com/lancedikson/bowser/blob/master/LICENSE