import { Logger } from './logger.js';
import { CurrentGeoLocationResponseModel } from "./location.js";

class MessageModel {
    v;
    mid;
    sid;
    message_type;
    from;
    to;
    created_at;
    message_content;
    conversation_type;
    other;

    constructor (message_content, from, to, mid) {
        this.v = MessageConstants.version;
        this.mid = mid ? mid : this.create_UUID();
        this.sid = this.create_UUID();
        this.message_type = MessageConstants.message_type;
        this.from = new UserModel(from);
        this.to = new UserModel(to);
        this.created_at = new Date().getTime();
        this.message_content = message_content;
        this.conversation_type = getConversationType(to);
        this.other = "";
    }

    create_UUID = () => {
        let dt = new Date().getTime();
        let uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            let r = (dt + Math.random() * 16) % 16 | 0;
            dt = Math.floor(dt / 16);
            return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
        return uuid;
    };

}

class UserModel {
    id;

    constructor (id) {
        if (id.indexOf("user:") > -1 || id.indexOf("interaction:") > -1 || id.indexOf("group:") > -1) {
            this.id = id.split(":")[ 1 ];
        }
        else {
            this.id = id;
        }
    }
}

class TypingStateModel {
    from;
    to;
    conversation_type;
    status;

    constructor (status, from, to) {
        this.from = from;
        this.to = this.FormatAddress(to);
        this.conversation_type = getConversationType(to);
        this.status = status;
    }

    FormatAddress = (to) => {
        if (to.indexOf("user:") > -1 || to.indexOf("interaction:") > -1) {
            return to.split(":")[ 1 ];
        }
        else {
            return to;
        }
    }
}

class ReadReceiptModel {
    from;
    to;
    conversation_type;
    mid;
    seen_at;
    v;
    constructor (mid, from, to) {
        this.v = MessageConstants.version;
        this.mid = mid;
        this.from = new UserModel(from);
        this.to = new UserModel(to);
        this.seen_at = new Date().getTime();
        this.conversation_type = getConversationType(to);
    }
}

const MessageConstants = {
    version: 1,
    message_type: "text/html"
}

function getConversationType(toAddress) {
    if (toAddress.indexOf("interaction:") > -1) {
        return 4;
    }
    else if (toAddress.indexOf("group:") > -1) {
        return 3;
    }
    else
        return 0;
}

class AppEventMessageProcessor {
    __isAppEventMessage;
    messageContent;
    constructor (messageContent) {
        if (messageContent != null && typeof messageContent === "object")
            if (messageContent.__isAppEventMessage != null && messageContent.__isAppEventMessage) {
                this.__isAppEventMessage = true;
                this.messageContent = messageContent;
            }
            else
                this.__isAppEventMessage = false;
        else
            this.__isAppEventMessage = false;
    }

    ProcessAppEvent = (livechat2api) => {
        if (this.__isAppEventMessage) {
            switch (this.messageContent.messageSubType) {
                case "request-current-location":
                    livechat2api.onCurrentLocationRequest(new CurrentGeoLocationResponseModel(this.messageContent.content, this.messageContent.requesterUserId, livechat2api));
                    break;
                case "response-current-location":
                    livechat2api.onCurrentLocationResponse(this.messageContent.content);
                    break;
                default: Logger.warn("Unknown messageSubType received: " + this.messageContent.messageSubType)
                    break;
            }
            return true;
        }
        else
            return false;
    }
}

class AppEventMessage {
    __isAppEventMessage;
    messageSubType;
    requesterUserId;
    content;
    constructor () {
        this.__isAppEventMessage = true;
        this.requesterUserId = "";
        this.messageSubType = "";
        this.content = {};
    }
}

export {
    MessageModel, UserModel, TypingStateModel, MessageConstants, ReadReceiptModel, AppEventMessage, AppEventMessageProcessor
};
