function sendUserMsg() {
    var chatMessage = $("#msg").val();
    var user = document.getElementById('msg-userId').value;
    if (chatMessage.trim() == "") {
        return;
    }
    else if (user === "") {
        return;
    }

    var uniqueId = create_UUID();
    writeToScreen(`Message Sent: ${chatMessage}`, uniqueId);

    livechatapi.sendTypingStateV2("STOP", user); //Send typing stopped to the remote user immediately when user submit the text.
    typingState = false;

    livechatapi.sendUserMessageV2(chatMessage, user, onSentCallback, uniqueId);
    document.getElementById('msg').value = '';
}

function onSentCallback(externalId, status) {
    logInfo(`${externalId}--${status}`);

    if (!status) {
        divContent = document.getElementById('div_' + externalId);
        var btnContent = "<button id='btn_" + externalId + "' onclick=resendUserMsg('" + externalId + "'); style='border:none;'><i class='fa fa-repeat' style='color:red' ></i></button>";
        divContent.innerHTML += btnContent;
    }

}

function resendUserMsg(externalId) {
    $("#msg").val(document.getElementById('' + externalId).innerHTML);
    sendUserMsg();
    document.getElementById('div_' + externalId).remove();
}


var txtMsg = document.getElementById('msg');

var getKeyCode = function (str) {
    return str.charCodeAt(str.length - 1);
}

let typingState = false; //Mantains the state, if the user is typing or not
let typingTimer = null; //A timer which is cleared and set everytime user starts typing

document.getElementById('msg').addEventListener('keyup', (e) => {  //Detect whenever user types in the text box
    if (typingState === false) { //If the user was not typing previously
        let id = document.getElementById('msg-userId').value
        if (id != null && id !== "")
            livechatapi.sendTypingStateV2("START", id); //Send typing start to the remote user
        typingState = true;
    }

    if (typingTimer != null) {
        clearTimeout(typingTimer); //Clear the existing timer
    }

    typingTimer = setTimeout(() => { //Add/update the timer
        if (typingState === true) { //If the user is currently typing and has stopped now
            let id = document.getElementById('msg-userId').value
            if (id != null && id !== "")
                livechatapi.sendTypingStateV2("STOP", id); //Send typing stopped to the remote user
            typingState = false;
        }
    }, 5000);
});

function create_UUID() {
    var dt = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (dt + Math.random() * 16) % 16 | 0;
        dt = Math.floor(dt / 16);
        return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
}

function onAppEvent(messageContent) {
    try {
        let parsedAppMessage = JSON.parse(messageContent);
        if (parsedAppMessage !== undefined) {
            if (parsedAppMessage.message_type !== undefined) {
                switch (parsedAppMessage.message_type) {
                    case "SCREENSHARE_REQUEST":
                        ProcessScreenshareRequest(messageContent);
                        break;
                    case "SCREENSHARE_REQUEST_TIMEOUT":
                        break;
                }
            }
        }
    } catch (error) {
        logError(`Error in onAppEvent`, error);
    }
}

let screenShareRequestTimer = null;

function ProcessScreenshareRequest(messageContent) {
    try {
        let screenshareAcceptEvent = 'AcceptScreenshareRequest(' + messageContent + ')';
        let screenshareRejectEvent = 'RejectScreenshareRequest(' + messageContent + ')';
        let ui = '<div class="modal fade" id="screenshareRequestModal" tabindex="-1" role="dialog" aria-labelledby="screenshareRequestModalLabel" aria-hidden="true">' +
            '<div class="modal-dialog">' +
            ' <div class="modal-content">' +
            ' <div class="modal-header">' +
            '<h5 class="modal-title" id="screenshareRequestModalLabel">Screenshare Request</h5>' +
            '</div>' +
            '<div class="modal-body"> You have received a screenshare request' +
            '</div>' +
            '<div class="modal-footer">' +
            '<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick=' + screenshareRejectEvent + '>Decline</button>' +
            '<button type="button" class="btn btn-primary" onclick=' + screenshareAcceptEvent + '>Accept</button>' +
            ' </div>' +
            ' </div>' +
            '</div>' +
            '</div>';
        var htmlObject = $(ui);
        document.body.appendChild(htmlObject[ 0 ]);
        $("#screenshareRequestModal").modal('show');
        screenShareRequestTimer = setTimeout(function () {
            RejectScreenshareRequest(JSON.parse(messageContent), true);
        }, 25000)
    } catch (error) {
        logError(`Error in ProcessScreenshareRequest`, error);
    }
}

function AcceptScreenshareRequest(messageContent) {
    try {
        $("#screenshareRequestModal").modal('hide');
        clearTimeout(screenShareRequestTimer);
        document.getElementById("screenshareRequestModal").remove();
        NavbarSwitch('audio-video');
        document.getElementById('toId').value = "user:" + messageContent.from.id;
        document.getElementById('screenshareStartButton').click();
    } catch (error) {
        logError(`Error in AcceptScreenshareRequest`, error);
    }
}

function RejectScreenshareRequest(messageContent, isTimedOut) {
    try {
        $("#screenshareRequestModal").modal('hide');
        clearTimeout(screenShareRequestTimer);
        let fromDetails = messageContent.to;
        let toDetails = messageContent.from;
        let message = messageContent;
        message.sid = create_UUID();
        message.mid = create_UUID();
        message.from = fromDetails;
        message.to = toDetails;
        if (isTimedOut) {
            message.message_content.command = 'SCREENSHARE_REQUEST_TIMEOUT';
        }
        else {
            message.message_content.command = 'REJECT_SCREENSHARE_REQUEST';
        }
        message.created_at = Date.now();
        livechatapi.sendAppEvent(JSON.stringify(message), "user:" + message.to.id);
        document.getElementById("screenshareRequestModal").remove();
    } catch (error) {
        logError(`Error in RejectScreenshareRequest`, error);
    }
}

function sendPhoneNotification() {
    let userId = document.getElementById("phone-notification-userId").value;
    let command = $("#phone-notification-command-button").text();
    let message = document.getElementById("phone-notification-message").value;
    let duration = document.getElementById("phone-notification-duration").value;

    if (userId == null || userId === "" || userId === "user:")
        return;
    else if (command == null || command === "" || command.toLowerCase() === "command")
        return;
    else if (message == null || message === "")
        return;

    let messageContent = {
        message_type: "PHONE_APP_NOTIFICATIONS",
        message_content: {
            command: command,
            message: message,
            duration: parseInt(duration)
        }
    }

    livechatapi.sendAppEvent(JSON.stringify(messageContent), userId);
    ShowNotification(notificationTypes.success, "Phone Notification Sent!");
}

document.getElementById("messaging-area-input-form").addEventListener("submit", function (event) {
    event.preventDefault();
    let date = new Date().getTime();

    var chatMessage = document.getElementById("messaging-area-input").value;

    if (!document.getElementById("messaging-area-input"))
        return;

    if (__interactionManager[ Object.keys(__interactionManager)[ 0 ] ].id == null || __interactionManager[ Object.keys(__interactionManager)[ 0 ] ].id == "")
        return;

    var uniqueId = create_UUID();
    writeToScreen(`Message Sent: ${chatMessage}`, uniqueId);

    livechatapi.sendTypingStateV2("STOP", __interactionManager[ Object.keys(__interactionManager)[ 0 ] ].id); //Send typing stopped to the remote user immediately when user submit the text.
    typingState = false;

    livechatapi.sendUserMessageV2(chatMessage, __interactionManager[ Object.keys(__interactionManager)[ 0 ] ].id, onSentCallback, uniqueId);

    renderMessage(`right`, chatMessage, date, null);
    document.getElementById("messaging-area-input").value = "";
});

document.getElementById('messaging-area-input-form').addEventListener('keyup', function (event) {  //Detect whenever user types in the text box
    if (typingState === false) { //If the user was not typing previously
        if (__interactionManager[ Object.keys(__interactionManager)[ 0 ] ].id != null && __interactionManager[ Object.keys(__interactionManager)[ 0 ] ].id !== "")
            livechatapi.sendTypingStateV2("START", __interactionManager[ Object.keys(__interactionManager)[ 0 ] ].id); //Send typing start to the remote user
        typingState = true;
    }

    if (typingTimer != null) {
        clearTimeout(typingTimer); //Clear the existing timer
    }

    typingTimer = setTimeout(() => { //Add/update the timer
        if (typingState === true) { //If the user is currently typing and has stopped now
            if (__interactionManager[ Object.keys(__interactionManager)[ 0 ] ].id != null && __interactionManager[ Object.keys(__interactionManager)[ 0 ] ].id !== "")
                livechatapi.sendTypingStateV2("STOP", __interactionManager[ Object.keys(__interactionManager)[ 0 ] ].id); //Send typing stopped to the remote user
            typingState = false;
        }
    }, 5000);
});

function renderMessage(side, message, unixTime, replyInfo = null) {
    try {
        var msgHTML = ``;

        if (replyInfo != null)
            msgHTML = `
                <div class="msg ${side}-msg">
                    <div class="msg-bubble">
                        <div class="msg-reply">
                            <div class="msg-reply-user">${replyInfo.user}</div>
                            <div class="msg-reply-text">${replyInfo.message}</div>
                        </div>
                        <div class="msg-text">${message}</div>
                        <div class="msg-info">
                            <div class="msg-info-name"></div>
                            <div class="msg-info-time">${generateTimeFromUnixTimestamp(unixTime)}</div>
                        </div>
                    </div>
                </div>`;
        else
            msgHTML = `
                <div class="msg ${side}-msg">
                    <div class="msg-bubble">
                        <div class="msg-text">${message}</div>
                        <div class="msg-info">
                        <div class="msg-info-name"></div>
                        <div class="msg-info-time">${generateTimeFromUnixTimestamp(unixTime)}</div>
                        </div>
                    </div>
                </div>`;

        document.getElementById("messaging-area").insertAdjacentHTML("beforeend", msgHTML);
        document.getElementById("messaging-area").scrollTop += 500;
        renderTypingMessage();
    } catch (error) {
        logError(`Error in renderMessage`, error);
    }
}

function renderIncomingMessage(content) {
    try {
        var replyInfo = null;
        var createdAt = content.created_at != null ? content.created_at : Date.now();

        if (content.related_message != null) {
            replyInfo = {
                message: "",
                user: ""
            };

            switch (content.message_type) {
                case "text/html":
                    replyInfo.message = content.related_message.message_content;
                    break;
                case "image":
                    replyInfo.message = "Image";
                    break;
                case "audio":
                    replyInfo.message = "Audio";
                    break;
                case "video":
                    replyInfo.message = "Video";
                    break;
            }

            if (content.related_message.from != null && content.related_message.from.id != null) {
                if (content.related_message.from.id === __userId)
                    replyInfo.user = "You";
                else
                    replyInfo.user = content.related_message.from.name != null ? content.related_message.from.name : content.related_message.from.id;
            }
        }

        renderMessage(`left`, content.message_content, createdAt, replyInfo);
    } catch (error) {
        logError(`Error in renderIncomingMessage`, error);
    }
}

function renderIncomingTypingStatus(content) {
    try {
        if (content != null && content.conversation_type === 4)
            if (content.status != null && content.status === "START")
                __interactionManager[ Object.keys(__interactionManager)[ 0 ] ].typingStatus = true;
            else
                __interactionManager[ Object.keys(__interactionManager)[ 0 ] ].typingStatus = false;

        renderTypingMessage();
    } catch (error) {
        logError(`Error in renderIncomingTypingStatus`, error);
    }
}

function renderTypingMessage() {
    try {
        $("#messaging-area > .typing-msg").remove();

        if (__interactionManager != null && Object.keys(__interactionManager).length > 0 && __interactionManager[ Object.keys(__interactionManager)[ 0 ] ].typingStatus === true) {
            var msgHTML = `
            <div class="msg left-msg typing-msg">
              <div class="msg-bubble">
                <div class="typing-container">
                    <div class="typing-block">
                        <div class="typing-dot"></div>
                        <div class="typing-dot"></div>
                        <div class="typing-dot"></div>
                    </div>
                </div>
              </div>
            </div>`;
            document.getElementById("messaging-area").insertAdjacentHTML("beforeend", msgHTML);
        }
    } catch (error) {
        logError(`Error in renderTypingMessage`, error);
    }
}

function renderNotificationMessage(message) {
    try {
        var notificationHTML = `
        <div class="messaging-notification">
            <div class="messaging-notification-bubble">
                <div class="messaging-notification-text">${message}</div>
            </div>
        </div>`;

        document.getElementById("messaging-area").insertAdjacentHTML("beforeend", notificationHTML);

        renderTypingMessage();
    } catch (error) {
        logError(`Error in renderNotificationMessage`, error);
    }
}

function resizeMessagingArea() {
    try {
        $(".messaging-section")[ 0 ].style.height = `calc(${window.innerHeight}px - ${$("#content-page header").height()}px - 60px)`;
    } catch (error) {
        logError(`Error in resizeMessagingArea`, error);
    }
}

function createInteraction(interactionId) {
    try {
        __interactionManager[ interactionId ] = {
            id: interactionId,
            typingStatus: false,
        };
        $("#messaging-area").empty();
        $("#messaging-area-input").attr("readonly", false);
        renderTypingMessage();
    } catch (error) {
        logError(`Error in createInteraction`, error);
    }
}

function clearAllInteractions() {
    try {
        __interactionManager = {};

        $("#messaging-area").empty();
        $("#messaging-area-input").attr("readonly", true);
        renderTypingMessage();
    } catch (error) {
        logError(`Error in clearAllInteractions`, error);
    }
}