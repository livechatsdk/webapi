
var callSdkGlobal = {};
var _isAudioMuted = false;
var _isVideoMuted = false;
var _acceptObject = null;

function AVLog(elem) {
	var this_ = this;
	var hasScrolled = false;

	this_.console_ = elem;

	this_.info = function (messageString) {
		logInfo(messageString);

		var html =
			`<div>
				<p class="card-text" style="margin-bottom: 8px;color: darkblue;">${messageString}</p>
			</div>`
		this_.console_.innerHTML += html;
	}

	this_.trace = function (messageString) {
		logInfo(messageString);

		var html =
			`<div>
				<p class="card-text" style="margin-bottom: 8px;color: gray;">${messageString}</p>
			</div>`
		this_.console_.innerHTML += html;
	}

	this_.error = function (code, msg) {
		logError(`AV Error: [${code}] - ${msg}`);

		var message = `[${code}] - ${msg}`
		var html =
			`<div>
				<p class="card-text" style="margin-bottom: 8px;color: darkred;">${message}</p>
			</div>`
		this_.console_.innerHTML += html;
	}

	this_.updateScroll = function () {
		if (!hasScrolled)
			this_.console_.scrollTop = this_.console_.scrollHeight;
	}

	this_.console_.removeEventListener('scroll', function () {
		hasScrolled = true;
	});

	this_.console_.addEventListener('scroll', function () {
		hasScrolled = true;
	});
}

function NodeJSChannel(addr) {
	var this_ = this;

	LiveChatAvCommInterface(this_);

	this_.socket_ = new WebSocket(addr);
	this_.uid_ = '' + Math.random();

	this_.onopen = function () { }
	this_.onclose = function () { }

	this_.isopen = function () {
		return (this_.socket_.readyState == WebSocket.OPEN);
	}

	this_.getUserId = function () {
		return this_.uid_;
	}

	this_.onVideoMessage = function (msg) { }

	this_.SendAppMessage = function (msg, to) {
		this_.socket_.send(msg);
	}

	this_.slog = function (type, unit, msg) { }

	this_.socket_.onopen = function (event) {
		callSdkGlobal.log.trace("web socket connected");
	}

	this_.socket_.onmessage = function (event) {
		callSdkGlobal.log.trace('message recieved ' + event.data);
		this_.onVideoMessage(event.data);
	}

	this_.socket_.onerror = function (event) {
		callSdkGlobal.log.error(0, "web socket error (" + event.reason + ")");
	}

	this_.socket_.onclose = function (event) {
		var error = event.reason;
		callSdkGlobal.log.trace("web socket closed");
		callSdkGlobal.log.trace('connection closed due to ' + error);
	}
}

function dummyChannel() {
	var this_ = this;

	this_.onopen = function () { }
	this_.onclose = function () { }

	this_.isopen = function () { return true; }

	setTimeout(function () {
		this_.onopen();
	}, 1000);
}

function MsSignalChannel(sessionId, address) {
	var this_ = this;

	LiveChatAvCommInterface(this_);

	this_.socket_ = new WebSocket(address);
	this_.sessionId_ = sessionId;
	this_.connected_ = false;
	this_.id_ = "n/a";

	this_.onopen = function () { }
	this_.onclose = function () { }

	//{@ Channel interface implementation
	this_.isopen = function () {
		return (this_.socket_.readyState == WebSocket.OPEN && this_.connected_);
	}

	this_.getUserId = function () {
		return this_.id_;
	}

	this_.onVideoMessage = function (msg) { }

	this_.SendAppMessage = function (msg, to) {
		var json = JSON.parse(msg);
		this_._send(json);
	}

	this_.slog = function (type, unit, msg) { }
	//@}

	this_._send = function (json) {
		json[ "from" ] = this_.id_;
		var jsonStr = JSON.stringify(json);
		this_.socket_.send(jsonStr);
	}

	this_.close = function () {
		this_.socket_.close();
	}

	this_.socket_.onopen = function (event) {
		callSdkGlobal.log.trace("web socket connected");

		var connectRequest = '{ "type" : "connect", "sessionId" : "test_session_' + this_.sessionId_ + '" }';
		this_.socket_.send(connectRequest);
		callSdkGlobal.log.trace("connecting to session " + this_.sessionId_);
	}

	this_.socket_.onmessage = function (event) {
		var json = JSON.parse(event.data);
		var frm = json[ "from" ];

		if (json.type == "connect") {
			this_.connected_ = true;
			this_.id_ = json.id;
			callSdkGlobal.log.trace('connect recieved');

			callSdkGlobal.log.trace('connected to chat session ' + this_.sessionId_ + ' as ' + this_.id_);

			this_.onopen();
		} else if (!this_.connected_) {
			callSdkGlobal.log.trace('message recieved prior to connection establishment ignored\nmessage: ' + event.data);
		} else if (json.to !== undefined && json.to != this_.id_) {
			callSdkGlobal.log.trace('message for other client (' + json.to + ') ignored\nmessage: ' + event.data);
		} else {
			this_.onVideoMessage(JSON.stringify(json));
		}
	}

	this_.socket_.onerror = function (event) {
		callSdkGlobal.log.trace("web socket error (" + event.reason + ")");
	}

	this_.socket_.onclose = function (event) {
		var error = event.reason;
		callSdkGlobal.log.trace("web socket closed");
		callSdkGlobal.log.trace('connection closed due to ' + error);
		this_.onclose();
	}
}

function createChannel(mode) {
	if (mode == 0) {
		return new dummyChannel();
	} else if (mode == 1) {
		wssAddr = "wss://" + location.hostname;
		if (location.port) {
			wssAddr = wssAddr + ":" + location.port;
		}
		return new NodeJSChannel(wssAddr);
	} else {
		wssAddr = "wss://" + location.hostname + ":8448/data";
		return new MsSignalChannel(1, wssAddr);
	}
}

function createCall(fa, ch, mode) {
	if (mode == 0) {
		try {
			return fa.CreateUsingLivechat2(livechatapi);
		} catch (e) {
			return null;
		}
	} else {
		return fa.CreateUsingCustom(ch);
	}
}

function updateUI(ev) {
	callSdkGlobal.callButton.enabled = false;
	callSdkGlobal.holdButton.enabled = false;
	callSdkGlobal.unholdButton.enabled = false;
	callSdkGlobal.hangupButton.enabled = false;
	callSdkGlobal.toIdTxt.readOnly = false;

	var isConnected = callSdkGlobal.channel.isopen();

	if (ev == 'connected') {
		callSdkGlobal.callButton.enabled = isConnected;
	} else if (ev == 'disconnected') {

	} else if (ev == 'calling') {
		callSdkGlobal.hangupButton.enabled = true;
	}
}

function onVideoMute() {
	try {
		var videoMuteButtons = document.getElementsByClassName("btn-sdk-video-mute");

		if (_isVideoMuted) {
			if (callSdkGlobal != null)
				callSdkGlobal.call.Pause(_isAudioMuted, false);
			_isVideoMuted = false;
		}
		else {
			if (callSdkGlobal != null)
				callSdkGlobal.call.Pause(_isAudioMuted, true);
			_isVideoMuted = true;
		}

		if (_isVideoMuted) {
			if (callSdkGlobal != null) {
				for (var i = 0; i < videoMuteButtons.length; i++) {
					videoMuteButtons[ i ].children[ 0 ].classList.remove("fa-video");
					videoMuteButtons[ i ].children[ 0 ].classList.add("fa-video-slash");
				}
			}
		}
		else {
			if (callSdkGlobal != null) {
				for (var i = 0; i < videoMuteButtons.length; i++) {
					videoMuteButtons[ i ].children[ 0 ].classList.add("fa-video");
					videoMuteButtons[ i ].children[ 0 ].classList.remove("fa-video-slash");
				}
			}
		}
	} catch (error) {
		logError(`Error in onVideoMute`, error);
	}
}

function onAudioMute() {
	try {
		var audioMuteButtons = document.getElementsByClassName("btn-sdk-audio-mute");

		if (_isAudioMuted) {
			if (callSdkGlobal != null)
				callSdkGlobal.call.Pause(false, _isVideoMuted);
			_isAudioMuted = false;
		}
		else {
			if (callSdkGlobal != null)
				callSdkGlobal.call.Pause(true, _isVideoMuted);
			_isAudioMuted = true;
		}

		if (_isAudioMuted) {
			if (callSdkGlobal != null) {
				for (var i = 0; i < audioMuteButtons.length; i++) {
					audioMuteButtons[ i ].children[ 0 ].classList.remove("fa-microphone");
					audioMuteButtons[ i ].children[ 0 ].classList.add("fa-microphone-slash");
				}
			}
		}
		else {
			if (callSdkGlobal != null) {
				for (var i = 0; i < audioMuteButtons.length; i++) {
					audioMuteButtons[ i ].children[ 0 ].classList.add("fa-microphone");
					audioMuteButtons[ i ].children[ 0 ].classList.remove("fa-microphone-slash");
				}
			}
		}
	} catch (error) {
		logError(`Error in onAudioMute`, error);
	}
}

function setInputDevice() {
	try {
		if (__selectedAudioInputDevice != null && __selectedVideoInputDevice != null) {
			return callSdkGlobal.call.SetInputDevice(__selectedAudioInputDevice.deviceId, __selectedVideoInputDevice.deviceId);
		}
		else if (__selectedVideoInputDevice != null) {
			return callSdkGlobal.call.SetInputDevice(null, __selectedVideoInputDevice.deviceId);
		}
		else if (__selectedAudioInputDevice != null) {
			return callSdkGlobal.call.SetInputDevice(__selectedAudioInputDevice.deviceId, null);
		}
	} catch (error) {
		logError(`Error in setInputDevice`, error);
		return false;
	}
}

function initCallSdk() {
	callSdkGlobal.videoContainer = document.getElementById('videoContainer');			//<- Div (fixed width and height)
	callSdkGlobal.consolePanel = document.getElementById('av-console');					//<- Div (v-scroll enabled fixed sized)
	callSdkGlobal.callButton = document.getElementById('callButton');					//<- Button
	callSdkGlobal.holdButton = document.getElementById('holdButton');					//<- Button
	callSdkGlobal.unholdButton = document.getElementById('unholdButton');
	callSdkGlobal.voiceMuteButton = document.getElementById('voiceMuteButton');					//<- Button
	callSdkGlobal.videoMuteButton = document.getElementById('videoMuteButton');					//<- Button
	callSdkGlobal.hangupButton = document.getElementById('hangupButton');				//<- Button
	callSdkGlobal.toIdTxt = document.getElementById('toId');							//<- Input Text
	callSdkGlobal.addButton = document.getElementById('addButton');				        //<- Button
	callSdkGlobal.screenshareVideoContainer = document.getElementById('screenshareVideoContainer');
	callSdkGlobal.screenshareStartButton = document.getElementById('screenshareStartButton');
	callSdkGlobal.screenshareEndButton = document.getElementById('screenshareStopButton');

	callSdkGlobal.signalMode = 0; // 0=livechat2, 1=MsSignal, 2=NodeSignal
	callSdkGlobal.wssAddr = "";
	callSdkGlobal.channel = createChannel(callSdkGlobal.signalMode);
	callSdkGlobal.log = new AVLog(callSdkGlobal.consolePanel);
	callSdkGlobal.callFactory = new LiveChatAvFactory();
	callSdkGlobal.call = createCall(callSdkGlobal.callFactory, callSdkGlobal.channel, callSdkGlobal.signalMode);
	callSdkGlobal.video = null;
	callSdkGlobal.screenshareVideo = null;

	callSdkGlobal.callButton.enabled = false;
	callSdkGlobal.holdButton.enabled = false;
	callSdkGlobal.unholdButton.enabled = false;
	callSdkGlobal.hangupButton.enabled = false;
	callSdkGlobal.toIdTxt.readOnly = false;

	callSdkGlobal.call.SetAvConfigs({
		MediaTransport: {
			iceServers: __webRTCConfigurations.iceServers,
			iceTransportPolicy: __webRTCConfigurations.iceTransportPolicy
		},
		MediaCapture: __webRTCConfigurations.mediaCapture,
		Network: __webRTCConfigurations.network,
		Display: {
			OnConnected: function (remoteStreams, localStreams, remoteUserInfo, callId) {
				if (callSdkGlobal.video != null) {
					callSdkGlobal.video.destroy();
				}

				callSdkGlobal.video = new VideoCallLayout(videoContainer, localStreams, remoteStreams);
			},
			OnStreamsChanged: function (remoteStreams, localStreams, remoteUserInfo, callId) {
				if (callSdkGlobal.video != null) {
					callSdkGlobal.video.destroy();
				}

				callSdkGlobal.video = new VideoCallLayout(videoContainer, localStreams, remoteStreams);
			},
			OnDisconnected: function (reasonCode, callId) {
				if (callSdkGlobal.video != null) {
					callSdkGlobal.video.destroy();
					callSdkGlobal.video = null;
				}
			},
			OnLocalHold: function (callId) {
			},
			OnLocalUnhold: function (callId) {
			},
			OnRemoteHold: function (callId) {
			},
			OnRemoteUnhold: function (callId) {
			},
			OnFail: function (errorCode, callId) {
				callSdkGlobal.log.error(errorCode, 'call error');

				if (__isCallRequestNotificationEnabled && document.getElementById("callRequestModal") != null) {
					//Hide the call request modal if it is displayed
					$("#callRequestModal").modal('hide');
					document.getElementById("callRequestModal").remove();
				}

			},
			OnRemoteRinging: function (userId, callId) {
			},
			OnIncomingCall: function (userIds, acceptObject, callId) {
				if (acceptObject.type() === "screenshare") {
					if (__isScreenshareRequestNotificationEnabled) {
						_acceptObject = acceptObject;
						let ui = '<div class="modal fade" id="screenshareRequestModal" tabindex="-1" role="dialog" aria-labelledby="screenshareRequestModalLabel" aria-hidden="true">' +
							'<div class="modal-dialog">' +
							' <div class="modal-content">' +
							' <div class="modal-header">' +
							'<h5 class="modal-title" id="screenshareRequestModalLabel">Screenshare Request</h5>' +
							'</div>' +
							'<div class="modal-body"> You have received a screenshare request' +
							'</div>' +
							'<div class="modal-footer">' +
							'<button type="button" class="btn btn-secondary" data-dismiss="modal" id="screenshareRequestModal-Decline">Decline</button>' +
							'<button type="button" class="btn btn-primary" id="screenshareRequestModal-Accept">Accept</button>' +
							' </div>' +
							' </div>' +
							'</div>' +
							'</div>';
						var htmlObject = $(ui);
						document.body.appendChild(htmlObject[ 0 ]);
						document.getElementById('screenshareRequestModal-Decline').addEventListener('click', function () {
							_acceptObject.reject();
							$("#screenshareRequestModal").modal('hide')
							document.getElementById("screenshareRequestModal").remove();
						});
						document.getElementById('screenshareRequestModal-Accept').addEventListener('click', function () {
							_acceptObject.accept();
							$("#screenshareRequestModal").modal('hide')
							document.getElementById("screenshareRequestModal").remove();
						});
						$("#screenshareRequestModal").modal('show');
					}
					else {
						acceptObject.accept();
					}
				}
				else {
					if (__isCallRequestNotificationEnabled) {
						_acceptObject = acceptObject;
						let ui = '<div class="modal fade" id="callRequestModal" tabindex="-1" role="dialog" aria-labelledby="callRequestModalLabel" aria-hidden="true">' +
							'<div class="modal-dialog">' +
							' <div class="modal-content">' +
							' <div class="modal-header">' +
							'<h5 class="modal-title" id="callRequestModalLabel">A/V Call Request</h5>' +
							'</div>' +
							'<div class="modal-body"> You have received a Audio/Video Call request' +
							'</div>' +
							'<div class="modal-footer">' +
							'<button type="button" class="btn btn-secondary" data-dismiss="modal" id="callRequestModal-Decline">Decline</button>' +
							'<button type="button" class="btn btn-primary" id="callRequestModal-Accept">Accept</button>' +
							' </div>' +
							' </div>' +
							'</div>' +
							'</div>';
						var htmlObject = $(ui);
						document.body.appendChild(htmlObject[ 0 ]);
						document.getElementById('callRequestModal-Decline').addEventListener('click', function () {
							_acceptObject.reject();
							$("#callRequestModal").modal('hide')
							document.getElementById("callRequestModal").remove();
						});
						document.getElementById('callRequestModal-Accept').addEventListener('click', function () {
							_acceptObject.accept();
							$("#callRequestModal").modal('hide')
							document.getElementById("callRequestModal").remove();
							NavbarSwitch('audio-video');
						});
						$("#callRequestModal").modal('show');
					}
					else {
						acceptObject.accept();
					}
				}
			},
			OnIncomingScreenshare: function (userIds, acceptObject, callId) {
				if (__isScreenshareRequestNotificationEnabled) {
					_acceptObject = acceptObject;
					let ui = '<div class="modal fade" id="screenshareRequestModal" tabindex="-1" role="dialog" aria-labelledby="screenshareRequestModalLabel" aria-hidden="true">' +
						'<div class="modal-dialog">' +
						' <div class="modal-content">' +
						' <div class="modal-header">' +
						'<h5 class="modal-title" id="screenshareRequestModalLabel">Screenshare Request</h5>' +
						'</div>' +
						'<div class="modal-body"> You have received a screenshare request' +
						'</div>' +
						'<div class="modal-footer">' +
						'<button type="button" class="btn btn-secondary" data-dismiss="modal" id="screenshareRequestModal-Decline">Decline</button>' +
						'<button type="button" class="btn btn-primary" id="screenshareRequestModal-Accept">Accept</button>' +
						' </div>' +
						' </div>' +
						'</div>' +
						'</div>';
					var htmlObject = $(ui);
					document.body.appendChild(htmlObject[ 0 ]);
					document.getElementById('screenshareRequestModal-Decline').addEventListener('click', function () {
						_acceptObject.reject();
						$("#screenshareRequestModal").modal('hide')
						document.getElementById("screenshareRequestModal").remove();
					});
					document.getElementById('screenshareRequestModal-Accept').addEventListener('click', function () {
						_acceptObject.accept();
						$("#screenshareRequestModal").modal('hide')
						document.getElementById("screenshareRequestModal").remove();
					});
					$("#screenshareRequestModal").modal('show');
				}
				else {
					acceptObject.accept();
				}
			},
			OnEvent: function (evData) {
				callSdkGlobal.log.info('Call Event: ' + evData.event);
			},
			OnScreenshareConnected: function (remoteStreams, remoteUserInfo, callId) {
				if (callSdkGlobal.screenshareVideo != null) {
					callSdkGlobal.screenshareVideo.destroy();
				}

				callSdkGlobal.screenshareVideo = new VideoCallLayout(screenshareVideoContainer, [], remoteStreams);
			},
			OnScreenshareDisconnected: function () {
				if (callSdkGlobal.screenshareVideo != null) {
					callSdkGlobal.screenshareVideo.destroy();
					callSdkGlobal.screenshareVideo = null;
				}
			},
			OnVoiceActivity: function (userId) {
				if (userId === "")
					logInfo(`No one is currently speaking`);
				else
					logInfo(`User ${userId} is currently speaking`);
			},
			OnSignalStrengthChange: function (strength, callId) {
				try {
					if (strength) {
						onSignalStrengthChange(strength);
					}
				} catch (error) {
					logError(`Error in initCallSdk - callSdkGlobal.call.SetAvConfigs - OnSignalStrengthChange`, error);
				}
			},
			OnRemoteTemporaryDisconnect: function (userId, callId) {
				try {
					var disconnectedUser = userId;
					if (disconnectedUser != null && disconnectedUser !== "" && __userId !== "") {
						if (disconnectedUser.indexOf(`user:`) > -1) {
							disconnectedUser = disconnectedUser.split(`:`)[ 1 ];
						}

						if (__userId === disconnectedUser)
							ShowNotification(notificationTypes.warning, "You have been disconnected from the call, trying to reconnect back!");
						else
							ShowNotification(notificationTypes.info, `User ${disconnectedUser} has been disconnected from the call`);
					}
				} catch (error) {
					logError(`Error in initCallSdk - callSdkGlobal.call.SetAvConfigs - OnRemoteTemporaryDisconnect`, error);
				}
			},
			OnRemoteReconnected: function (userId, callId) {
				try {
					var reconnectedUser = userId;
					if (reconnectedUser != null && reconnectedUser !== "" && __userId !== "") {
						if (reconnectedUser.indexOf(`user:`) > -1) {
							reconnectedUser = reconnectedUser.split(`:`)[ 1 ];
						}

						if (__userId === reconnectedUser)
							ShowNotification(notificationTypes.success, "You have been reconnected back to the call!");
						else
							ShowNotification(notificationTypes.info, `User ${reconnectedUser} has been reconnected to the call`);
					}
				} catch (error) {
					logError(`Error in initCallSdk - callSdkGlobal.call.SetAvConfigs - OnRemoteReconnected`, error);
				}
			},
		},
		VoiceActivity: __webRTCConfigurations.voiceActivity,
		AudioVideoConnectionMode: __webRTCConfigurations.audioVideoConnectionMode,
		TryToMergeSingleTrackStreamsFromDifferentSources: __webRTCConfigurations.tryToMergeSingleTrackStreamsFromDifferentSources,
	});

	callSdkGlobal.channel.onopen = function () {
		updateUI('connected');
	}

	callSdkGlobal.channel.onclose = function () {
		updateUI('disconnected');
	}

	callSdkGlobal.callButton.onclick = function () {
		try {
			if (isVideoCall != null) {
				setInputDevice();

				var to = callSdkGlobal.toIdTxt.value;

				if (to != "") {
					if (isVideoCall) {
						callSdkGlobal.call.StartAvCall('2', to, { call: LiveChatAvCallTypes.VIDEO, startAudioMuted: _isAudioMuted, startVideoMuted: _isVideoMuted });
					}
					else {
						callSdkGlobal.call.StartAvCall('1', to, { call: LiveChatAvCallTypes.AUDIO, startAudioMuted: _isAudioMuted, startVideoMuted: _isVideoMuted });
					}
					updateUI('calling');
				}
			}
			else {
				ShowNotification(notificationTypes.error, "Select the Call Type (Audio/Video)");
			}
		} catch (error) {
			logError(`Error in initCallSdk - callSdkGlobal.callButton`, error);
		}
	}

	callSdkGlobal.addButton.onclick = function () {
		var to = callSdkGlobal.toIdTxt.value;
		callSdkGlobal.toIdTxt.value = "";

		if (to != "") {
			callSdkGlobal.call.Conference(to);
		}
	}

	callSdkGlobal.holdButton.onclick = function () {
		callSdkGlobal.call.Hold(true);
	}

	callSdkGlobal.unholdButton.onclick = function () {
		callSdkGlobal.call.Hold(false);
	}

	callSdkGlobal.voiceMuteButton.onclick = onAudioMute;

	callSdkGlobal.videoMuteButton.onclick = onVideoMute;

	callSdkGlobal.hangupButton.onclick = function () {
		try {
			if (__channel != null && __channel === "video") { //Check if it is a click to call
				if (disconnectChatOnClickToCallEnd != null && disconnectChatOnClickToCallEnd) {
					//If this is a click to call and if it is configured to end the interaction on then we call end
					end('CLIENT_CLICK_TO_CALL_END');
				}
				else {
					callSdkGlobal.call.EndAvCall();
					callSdkGlobal.video.destroy();
					callSdkGlobal.video = null;
				}
			}
			else {
				callSdkGlobal.call.EndAvCall();
				callSdkGlobal.video.destroy();
				callSdkGlobal.video = null;
			}
		} catch (error) {
			logError(`Error in initCallSdk - callSdkGlobal.hangupButton`, error);
		}
	}

	callSdkGlobal.screenshareStartButton.onclick = function () {
		var to = callSdkGlobal.toIdTxt.value;

		if (to != "") {
			callSdkGlobal.call.StartScreensharing('1', to);
		}
	}

	callSdkGlobal.screenshareEndButton.onclick = function () {
		callSdkGlobal.call.StopScreensharing();
		if (callSdkGlobal.screenshareVideo != null) {
			callSdkGlobal.screenshareVideo.destroy();
			callSdkGlobal.screenshareVideo = null;
		}
	}

	setInputDevice();
}

//setTimeout(initCallSdk, 1000);