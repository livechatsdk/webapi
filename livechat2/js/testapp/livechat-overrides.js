//import { Livechat2Api } from '../api/livechat2api.js';

//window.livechatapi = new Livechat2Api("wss://" + livechatApiSocketDomain);

/*
    For minified Livechat2Sdk version, please use below SDK initialization code and comment above 2 lines. 
    Also include livechat2sdk-lib.min.js file in testapp2.html 
*/
window.livechatapi = new Livechat2Sdk.Livechat2Api("wss://" + livechatApiSocketDomain, config);

logInfo(writeToScreen(`Livechat API Version: ${livechatapi.getVersion()}`));

//Enable SDK console and debug logging, but disable masking
livechatapi.sdkLoggingConfigurations(__logConfigurations.sdk.enableConsoleLogging, __logConfigurations.sdk.enableDebugConsoleLogs, __logConfigurations.sdk.enableLogMasking);

livechatapi.onEnd = reason => {
    logInfo(writeToScreen("Session Ended: " + reason));
    if (callSdkGlobal != null && callSdkGlobal.call != null)
        callSdkGlobal.call.EndAvCall();
    ChangePageContent(false);
    ShowNotification(notificationTypes.info, `Session Ended (${reason})`);
}

livechatapi.onUserMessage = (content, from) => {
    logInfo(`User Message Received: ${content}`);
    writeToScreen(`User Message Received: ${JSON.parse(content).message_content}`);

    let contentJSON = JSON.parse(content);
    let to = "";

    if (contentJSON.conversation_type == null) {
        //This means this is an enterprise user-message (coming from chat server)
        renderIncomingMessage(contentJSON);
    }
    else if (contentJSON.conversation_type === 4) { //Check if it is an interaction-message
        to = `interaction:${contentJSON.to ? contentJSON.to.id : ""}`;
        renderIncomingMessage(contentJSON);
        livechatapi.sendReadReciptV2(contentJSON.mid, to, `user:${contentJSON.from ? contentJSON.from.id : ""}`);
    } else if (contentJSON.conversation_type === 3) { //Check if it is an group-message
        to = `group:${contentJSON.to ? contentJSON.to.id : ""}`;
        livechatapi.sendReadReciptV2(contentJSON.mid, to, `user:${contentJSON.from ? contentJSON.from.id : ""}`);
    } else { //Else we assume it is a one-one (user-user) message
        to = `user:${contentJSON.from ? contentJSON.from.id : ""}`;
        livechatapi.sendReadReciptV2(contentJSON.mid, to, null);
    }
}

livechatapi.onTypingState = (content, from) => {
    renderIncomingTypingStatus(content);
    if (content.status === "START")
        showTyping();
    else
        hideTyping();
}

livechatapi.onStartSuccess = (sid, cnt) => {
    logInfo(`onStartSuccess: ${sid} ${(cnt != null ? JSON.stringify(cnt) : "")}`);
    writeToScreen(`Session Started (${sid})`);
    if (cnt != null && cnt.tchatid != null) writeToScreen(`Chat Session Id (${cnt.tchatid})`);
    ChangePageContent(true);
    ShowNotification(notificationTypes.success, "Session Started");
    if (bStartWithoutLogin === true && cnt == null) {
        $('#findUserModal').modal('show');
    }
    else if (cnt.tchatid != null) {
        document.getElementById('toId').value = "user:" + cnt.tchatid.split('_')[ 1 ];
    }

    initCallSdk();
}

livechatapi.onStartFailed = error => {
    logInfo(`onStartFailed: ${error}`);
    writeToScreen(`Session Start Failed (${error})`);
    ShowNotification(notificationTypes.error, "Session Start Failed");

    var json = JSON.parse(error);
    if (json.reasonCode === 'SESSION_EXIST') {
        $('#sessionModal').modal('show');
    }
}

livechatapi.onSignatureRequest = () => {
    logInfo(writeToScreen("Agent requested signature"));
    document.getElementById("signature-pad").style.display = "block";
    $('#sigPadModal').modal('show');
    setTimeout(function () {
        signaturePad = new SignaturePad(canvas);
        resizeCanvas();
    }, 500);
}

livechatapi.onRequestQueued = info => {
    logInfo(`Request Queued, details: ${JSON.stringify(info)}`);
    writeToScreen(`Request Queued`)
    ShowNotification(notificationTypes.success, "Request Queued");

    //Dont show the EWT Status button for enterprise environment
    if (info.position != null && info.position !== -1)
        ShowEWTStatusButton(true);
}

livechatapi.onSystemNotification = notification => {
    logInfo(writeToScreen(`System Notification: ${notification.reasonCode}`))
    ShowNotification(notificationTypes.error, `System Notification: ${notification.reasonCode}`);
}

livechatapi.onUserFound = (userFoundData) => {
    try {
        logInfo(`User Found with details:\n ${JSON.stringify(userFoundData)}`);
        if (userFoundData.userId != null && userFoundData.userId !== "")
            writeToScreen(`User Found (${userFoundData.userId})`);
        else if (userFoundData.foundUser != null && userFoundData.foundUser !== "")
            writeToScreen(`User Found (${userFoundData.foundUser})`);
        ShowNotification(notificationTypes.success, "User Found");

        if (userFoundData.interactionId != null) {
            createInteraction(`interaction:${userFoundData.interactionId}`);
            document.getElementById('msg-userId').value = "interaction:" + userFoundData.interactionId;
            document.getElementById('toId').value = "interaction:" + userFoundData.interactionId;
        }
        else if (userFoundData.foundUser != null && userFoundData.foundUser !== "") {
            document.getElementById('msg-userId').value = "user:" + userFoundData.foundUser;
            document.getElementById('toId').value = "user:" + userFoundData.foundUser;
        }
        else if (userFoundData.userId != null && userFoundData.userId !== "") {
            document.getElementById('msg-userId').value = "user:" + userFoundData.userId;
            document.getElementById('toId').value = "user:" + userFoundData.userId;
        }

        $('#findUserModal').modal('hide');
        document.getElementById("btnFindUser").disabled = false;

        if (__channel === "video") {
            setTimeout(function () {
                NavbarSwitch('audio-video');
                OnSelectAudioVideo(true);
                callSdkGlobal.callButton.click();
            }, 500);
        }
    } catch (error) {
        logError(`Error in onUserFound`, error);
    }
    ShowEWTStatusButton(false);
}

livechatapi.onUserNotFound = (notfoundData) => {
    logInfo(`User could not be found with details:\n${JSON.stringify(notfoundData)}`);
    writeToScreen(`User Not Found`);
    ShowNotification(notificationTypes.error, "User Not Found");
    $('#findUserModal').modal('hide');
    ShowEWTStatusButton(false);
}

livechatapi.onGroupCreated = groupInfo => {
    logInfo(`onGroupCreated: Group created with below parameters:\n Group ID:${groupInfo.Id}\n Group Name:${groupInfo.Name}\n Group Description: ${groupInfo.Description}\n Created By: ${groupInfo.CreatedBy}\n Created Date Time: ${groupInfo.CreatedDateTime}\n Group Participants: ${JSON.stringify(groupInfo.Participants)}`);
    writeToScreen(`onGroupCreated`);
    AddGroupToUI(groupInfo);
}

livechatapi.onGroupDeleted = groupId => {
    logInfo(`Group ${groupId} is deleted`);
    writeToScreen(`onGroupDeleted`)
}

livechatapi.onUserAddedToGroup = groupInfo => {
    logInfo(`Group created with below parameters:\n Group ID:${groupInfo.Id}\n Group Name:${groupInfo.Name}\n Group Description: ${groupInfo.Description}\n Created By: ${groupInfo.CreatedBy}\n Created Date Time: ${groupInfo.CreatedDateTime}\n Group Participants: ${groupInfo.Participants.toString()}`);
    writeToScreen(`onUserAddedToGroup`);
}

livechatapi.onUserRemovedFromGroup = userInfo => {
    logInfo("Users " + userInfo.Participants.toString() + " are removed from group with Id " + userInfo.GroupId + " at " + userInfo.ParticipantsModifiedDateTime);
    writeToScreen(`onUserRemovedFromGroup`);
}

livechatapi.onGroupNameModified = groupInfo => {
    logInfo("Name of the Group with Id " + groupInfo.Id + " has been changed to " + groupInfo.Name);
    writeToScreen(`onGroupNameModified`);
}

livechatapi.onGroupDescriptionModified = groupInfo => {
    logInfo(writeToScreen("Name of the Group with Id " + groupInfo.Id + " has been changed to " + groupInfo.Description));
    writeToScreen(`onGroupDescriptionModified`);
}

livechatapi.onUserGroupListing = groupListInfo => {
    logInfo("Group List received for user Id: " + groupListInfo.UserId + " \n Last Group Id: " + groupListInfo.LastGroupListId + " \n Group List Information: " + JSON.stringify(groupListInfo.GroupListInfo));
    writeToScreen(`onUserGroupListing`);
    AddGroupListsToUI(groupListInfo);
}

livechatapi.onGroupMessageReceived = groupMessageDetails => {
    logInfo("Group Message Received with details: " + JSON.stringify(groupMessageDetails));
    writeToScreen(`onGroupMessageReceived`);
}

livechatapi.onGroupAdminAdded = info => {
    logInfo("User  " + info.UserId + " has been made an admin in group " + info.GroupId);
    writeToScreen(`onGroupAdminAdded`);
}

livechatapi.onGroupAdminDismissed = info => {
    logInfo("User  " + info.UserId + " has been dismissed as admin from group " + info.GroupId);
    writeToScreen(`onGroupAdminDismissed`);
}

livechatapi.onUserLeftGroup = info => {
    logInfo("User  " + info.UserId + " has left group " + info.GroupId);
    writeToScreen(`onUserLeftGroup`);
}

livechatapi.onGroupIconChanged = info => {
    logInfo("Group " + info.GroupId + " icon has changed (" + info.IconPath + ")");
    writeToScreen(`onGroupIconChanged`);
}

livechatapi.onServerStateChange = state => {
    logInfo("Server state changed to " + (state === true ? "online" : "offline"));
    writeToScreen(`onServerStateChange`);
}

livechatapi.onConnect = () => {
    if (__serverConnectionStatus == undefined || __serverConnectionStatus !== "connected") {
        __serverConnectionStatus = "connected";
        logInfo(writeToScreen("Connection Status: Connected"));
        ShowNotification(notificationTypes.success, "Connected");
    }
};

livechatapi.onReconnect = () => {
    if (__serverConnectionStatus == undefined || __serverConnectionStatus !== "reconnecting") {
        __serverConnectionStatus = "reconnecting";
        logInfo(writeToScreen("Connection Status: Reconnecting"));
        ShowNotification(notificationTypes.success, "Reconnected");
    }
};

livechatapi.onDisconnect = reason => {
    if (__serverConnectionStatus == undefined || __serverConnectionStatus !== "disconnected") {
        __serverConnectionStatus = "disconnected";
        if (reason != null && reason !== "")
            logInfo(writeToScreen(`Connection Status: Disconnected (${reason})`));
        else
            logInfo(writeToScreen(`Connection Status: Disconnected`));
        ShowNotification(notificationTypes.error, "Disconnected");
    }
};

livechatapi.onAppEvent = content => {
    onAppEvent(content);
}

livechatapi.onInteractionTransferSuccess = info => {
    logInfo("Interaction: " + info.InteractionId + " has been successfully transferred from user: " + info.RequestorUserId + " to user: " + info.UserId);
    writeToScreen(`onInteractionTransferSuccess`);
    ShowNotification(notificationTypes.success, "Interaction (" + info.InteractionId + ") transferred to Agent: " + info.UserId);
    renderNotificationMessage(`User ${info.RequestorUserId} has transferred the interaction to User ${info.UserId}`);
}

livechatapi.onInteractionConferenceSuccess = info => {
    writeToScreen(`onInteractionConferenceSuccess`);
    if (info.IsChat) {
        logInfo("User: " + info.UserId + " has been successfully conferenced to the interaction: " + info.InteractionId);
        ShowNotification(notificationTypes.success, `User ${info.UserId} is conferenced to interaction: ${info.InteractionId}`);
        renderNotificationMessage(`User ${info.RequestorUserId} added User ${info.UserId}`);
    }
    else if (info.IsChat === false && info.IsCall) {
        logInfo(writeToScreen("User: " + info.UserId + " has been successfully conferenced to the Call"));
        ShowNotification(notificationTypes.success, "User: " + info.UserId + " conferenced to Call");
    }
}

livechatapi.onInteractionEnd = info => {
    logInfo(writeToScreen("Interaction: " + info.InteractionId + " has ended with reason: " + info.Reason));
    ShowNotification(notificationTypes.info, "Interaction: " + info.InteractionId + " has ended with reason: " + info.Reason);

    //End the Session, since the interaction has ended
    end('INTERACTION_END');
}

livechatapi.onUserInteractionListing = info => {
    logInfo(`User: ${info[ 0 ].UserId} has received interaction list: ${JSON.stringify(info)}`);
    writeToScreen(`onUserInteractionListing`);

    if (info[ 0 ].InteractionListInformation !== null) {
        var data = info[ 0 ].InteractionListInformation[ 0 ];

        document.getElementById('msg-userId').value = "interaction:" + data.InteractionId;
        document.getElementById('toId').value = "interaction:" + data.InteractionId;

        createInteraction(`interaction:${data.InteractionId}`);
    }
}

livechatapi.onReadRecipt = (receipt, from) => {
    logInfo("Read receipt received: " + JSON.stringify(receipt));
    writeToScreen(`onReadRecipt`);
}

livechatapi.onDeliveryRecipt = (receipt, from) => {
    logInfo("Delivery receipt received: " + JSON.stringify(receipt));
    writeToScreen(`onDeliveryRecipt`);
}

livechatapi.onEWTStatus = responseObj => {
    logInfo("Details for Estimated Wait Time received: " + JSON.stringify(responseObj));
    writeToScreen(`onEWTStatus`);
}

livechatapi.onCurrentLocationRequest = content => {
    logInfo("Current Location Request received. Accepting Request... ");
    writeToScreen(`onCurrentLocationRequest`);
    content.accept();
}

livechatapi.onCurrentLocationResponse = content => {
    logInfo("User (" + content.userId + ") location details received: " + JSON.stringify(content));
    writeToScreen(`onCurrentLocationResponse`);
}

livechatapi.onFileUploadSuccess = responseObj => {
    logInfo(writeToScreen(`File Uploaded Successfully: ${JSON.stringify(responseObj)}`));
    ShowNotification(notificationTypes.success, "File Uploaded Successfully");
}

livechatapi.onFileUploadFailure = responseObj => {
    logInfo(writeToScreen(`File Uploaded Failed: ${JSON.stringify(responseObj)}`));
    ShowNotification(notificationTypes.success, "File Upload Failed");
}