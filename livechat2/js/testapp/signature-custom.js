var wrapper = document.getElementById("signature-pad");
var clearButton = document.getElementById("signature-pad-clear");
var cancelButton = document.getElementById("signature-pad-cancel");
var savePNGButton = document.getElementById("signature-pad-submit");
//var saveSVGButton = wrapper.querySelector("[data-action=save-svg]");
var canvas = document.getElementById("signature-pad-canvas");
//var signaturePad = new SignaturePad(canvas);
var signaturePad;

// Adjust canvas coordinate space taking into account pixel ratio,
// to make it look crisp on mobile devices.
// This also causes canvas to be cleared.
function resizeCanvas() {
	if(wrapper.style.display!="none"){
    // When zoomed out to less than 100%, for some very strange reason,
    // some browsers report devicePixelRatio as less than 1
    // and only part of the canvas is cleared then.
    var ratio = Math.max(window.devicePixelRatio || 1, 1);

    // This part causes the canvas to be cleared
    canvas.width = canvas.offsetWidth * ratio;
    canvas.height = canvas.offsetHeight * ratio;
    canvas.getContext("2d").scale(ratio, ratio);

    // This library does not listen for canvas changes, so after the canvas is automatically
    // cleared by the browser, SignaturePad#isEmpty might still return false, even though the
    // canvas looks empty, because the internal data of this library wasn't cleared. To make sure
    // that the state of this library is consistent with visual state of the canvas, you
    // have to clear it manually.
    if(signaturePad) signaturePad.clear();
	}
}

window.onresize = resizeCanvas;

clearButton.addEventListener("click", function (event) {
    signaturePad.clear();
});

cancelButton.addEventListener("click", function (event) {
    //parent.signatureReject();
	signaturePad.clear();
	wrapper.style.display="none";
	
});

savePNGButton.addEventListener("click", function (event) {
    if (signaturePad.isEmpty()) {
        alert("Please provide signature first.");
    } else {
        //window.open(signaturePad.toDataURL());
        saveimage(signaturePad.toDataURL());
		wrapper.style.display="none";
        $('#sigPadModal').modal('hide');
    }
});

// url: "WS_Signature.asmx/UploadFile",
function saveimage(dataURL) {
	if(livechatapi){
		fetch(dataURL)
                .then(res => res.blob())
                .then(blob => {
					const now = new Date();
                    let dateTime = now.getFullYear().toString() + (("0" + (now.getMonth() + 1).toString()).slice(-2)) + (("0" + now.getDate().toString()).slice(-2)) + (("0" + now.getHours().toString()).slice(-2)) + (("0" + now.getMinutes().toString()).slice(-2)) + (("0" + now.getSeconds().toString()).slice(-2));
                    const file = new File([blob], "Signature-" + dateTime + ".png", { type: "image/png" })
					livechatapi.uploadSignature([file]);
                });
	}
}