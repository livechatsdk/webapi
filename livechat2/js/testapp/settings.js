function InitializeDefaultSettings() {
    try {
        if (__defaultSettings.showSettingsPage === true)
            document.getElementById(`settings-nav-item`).classList.remove(`hidden`);
        else
            document.getElementById(`settings-nav-item`).classList.add(`hidden`);

        $('.settings-tooltip-button').tooltip();

        if (__defaultSettings.general != null)
            if (__defaultSettings.general.showConsole != null)
                ConsoleLoggingToggle(true);

        if (__defaultSettings.messaging != null) {
            if (__defaultSettings.messaging.showSimulateNotificationsSection != null)
                SimulatePhoneNotificationsToggle(true);

            if (__defaultSettings.messaging.showNewMessagingUI != null)
                NewMessagingUI(true);
        }

        if (__defaultSettings.groups != null)
            if (__defaultSettings.groups.enabled != null)
                GroupSectionToggle(true);

        if (__defaultSettings.av != null)
            if (__defaultSettings.av.showAVConsole != null)
                AVConsoleLoggingToggle(true);
    } catch (error) {
        logError(`Error in InitializeDefaultSettings`, error);
    }
}

function ConsoleLoggingToggle(override = false) {
    try {
        var isConsoleLoggingEnabled = false;

        if (override === true) {
            isConsoleLoggingEnabled = !__defaultSettings.general.showConsole;
            if (__defaultSettings.general.showConsole === true)
                document.getElementById('console-logging-toggle').classList.add(`active`);
            else
                document.getElementById('console-logging-toggle').classList.remove(`active`);
        }
        else
            isConsoleLoggingEnabled = document.getElementById('console-logging-toggle').classList.contains("active");

        if (isConsoleLoggingEnabled === true) {
            document.getElementById('testapp-navs-console').classList.add('hidden');
            document.getElementById('testapp-navs-content').classList.remove('col-md-8');
            document.getElementById('testapp-navs-content').classList.add('col-md-12');
        }
        else {
            document.getElementById('testapp-navs-console').classList.remove('hidden');
            document.getElementById('testapp-navs-content').classList.add('col-md-8');
            document.getElementById('testapp-navs-content').classList.remove('col-md-12');
        }
    } catch (error) {
        logError(`Error in ConsoleLoggingToggle`, error);
    }
}

function AVConsoleLoggingToggle(override = false) {
    try {
        var isAVConsoleLoggingEnabled = false;

        if (override === true) {
            isAVConsoleLoggingEnabled = !__defaultSettings.av.showAVConsole;
            if (__defaultSettings.av.showAVConsole === true)
                document.getElementById('av-console-logging-toggle').classList.add(`active`);
            else
                document.getElementById('av-console-logging-toggle').classList.remove(`active`);
        }
        else
            isAVConsoleLoggingEnabled = document.getElementById('av-console-logging-toggle').classList.contains("active");

        if (isAVConsoleLoggingEnabled === true)
            document.getElementById('av-console-div').classList.add('hidden');
        else
            document.getElementById('av-console-div').classList.remove('hidden');
    } catch (error) {
        logError(`Error in AVConsoleLoggingToggle`, error);
    }
}

function GroupSectionToggle(override = false) {
    try {
        var isGroupSectionEnabled = false;

        if (override === true) {
            isGroupSectionEnabled = !__defaultSettings.groups.enabled;
            if (__defaultSettings.groups.enabled === true)
                document.getElementById('group-section-toggle').classList.add(`active`);
            else
                document.getElementById('group-section-toggle').classList.remove(`active`);
        }
        else
            isGroupSectionEnabled = document.getElementById('group-section-toggle').classList.contains("active");

        if (isGroupSectionEnabled === true)
            document.getElementById('groups-nav-item').classList.add('hidden');
        else
            document.getElementById('groups-nav-item').classList.remove('hidden');
    } catch (error) {
        logError(`Error in GroupSectionToggle`, error);
    }
}

function SimulatePhoneNotificationsToggle(override = false) {
    try {
        var isSimulatePhoneNotificationsEnabled = false;

        if (override === true) {
            isSimulatePhoneNotificationsEnabled = !__defaultSettings.messaging.showSimulateNotificationsSection;
            if (__defaultSettings.messaging.showSimulateNotificationsSection === true)
                document.getElementById('simulate-phone-notifications-toggle').classList.add(`active`);
            else
                document.getElementById('simulate-phone-notifications-toggle').classList.remove(`active`);
        }
        else
            isSimulatePhoneNotificationsEnabled = document.getElementById('simulate-phone-notifications-toggle').classList.contains("active");

        if (isSimulatePhoneNotificationsEnabled === true)
            document.getElementById('simulate-phone-notifications-div').classList.add('hidden');
        else
            document.getElementById('simulate-phone-notifications-div').classList.remove('hidden');
    } catch (error) {
        logError(`Error in SimulatePhoneNotificationsToggle`, error);
    }
}

function NewMessagingUI(override = false) {
    try {
        var isNewMessagingUIEnabled = false;

        if (override === true) {
            isNewMessagingUIEnabled = !__defaultSettings.messaging.showNewMessagingUI;
            if (__defaultSettings.messaging.showNewMessagingUI === true)
                document.getElementById('new-messaging-ui-toggle').classList.add(`active`);
            else
                document.getElementById('new-messaging-ui-toggle').classList.remove(`active`);
        }
        else
            isNewMessagingUIEnabled = document.getElementById('new-messaging-ui-toggle').classList.contains("active");

        if (isNewMessagingUIEnabled === true) {
            document.getElementById('messaging-secondary').classList.add('hidden');
            document.getElementById('messaging-primary').classList.remove('hidden');
        }
        else {
            document.getElementById('messaging-primary').classList.add('hidden');
            document.getElementById('messaging-secondary').classList.remove('hidden');
        }
    } catch (error) {
        logError(`Error in NewMessagingUI`, error);
    }
}