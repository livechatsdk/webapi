var userList = []

var livechatApiSocketDomain = location.host;
//var livechatApiSocketDomain = "test.tetherficloud.com:15012"; //Cloud Dev
//var livechatApiSocketDomain = "proxy.lab.tetherfi.cloud"; // IDFC LAB
//var livechatApiSocketDomain = "test.tetherficloud.com:22533"; //Enterprise Dev
//var livechatApiSocketDomain = "dice.tetherfi.cloud:8443"; //DICE

var useTenancyBasedLogin = false;

var loginAuthURL = "";
//var loginAuthURL = "https://test.tetherficloud.com:8125"; //Cloud Dev
//var loginAuthURL = "https://proxy.lab.tetherfi.cloud"; // IDFC LAB

//URL Configs
var config = {
    logUrl: `https://${livechatApiSocketDomain}`
};

var videoSignalStrengthMessages = {
    enabled: true, //to enable/disable signal strength notifications
    notificationInterval: 5000 //This is the interval (in milliseconds) at which signal strength notifications will appear
}

var __isScreenshareRequestNotificationEnabled = true; //to enable accept/decline of incoming screenshare request

var __isCallRequestNotificationEnabled = true; //to enable accept/decline of incoming a/v call request

var authType = "idfc"; //Possible values are idfc, cloud
var sessionId = "3a600342-a7a3-4c66-bbd3-f67de5d7096f";
var __disableAuthTokenValidation = false; //If set to false, customerInfo will be sent as blank

var authToken;
switch (authType) {
    case "cloud": //auth token for cloud
        authToken = "eyJhbGciOiJIUzUxMiJ9.eyJhdWQiOiJzYW5kZWVwQHRldGhlcmZpLmNvbSIsIm9yZ2FuaXphdGlvbiI6InRldGhlcmZpIiwiaWQiOiJzYW5kZWVwIiwiZXhwIjoxNjExNTU5NzU0LCJpYXQiOjE2MTA1MjA1MjUsIm9yZ0lkIjoiMSIsImp0aSI6ImY0ZjYwYmRhLTYxNzktNGFhZi1hMzM4LTk4YzNiYmE0ZTVhZSJ9.6tTM0JRB93FzsSnXXrDwImCbmlV11GaS5v7c31Zd2sS46YixOlpCSzP5DmJvj9Cvz4qYWizDI4rP7xKZMAOM1Q";
        break

    case "idfc": //auth token for idfc
        authToken = "eyJlbmMiOiJBMjU2R0NNIiwiYWxnIjoiUlNBLU9BRVAtMjU2In0.i9dOua2pbmNri59TEriWltR0sUQnf8vsErT-5DhoK_XsEnKcKFU1n2R2h---fuMR-MbrTCe3K2AmVW6zEMktQB-WOSDzzHYLt-LttIk0bgiTKSixF8AuaTTk-Dx1JetiW9AZ9aZn6iJis8XG1VxO00EF2OZbsMM-jCd_xKjf9ydz6pxQun6tn9C81buvmhVs8-W4r7noS1A4D2Auskdu0xXl4Q_eUuoMDQE2eyJXEVL8CcwctAUQL1u4wix1t8Zt_SnxiavCQM3pV0A1kesRJyZFCOS2RKvKMBKjF7TFfy0MSoJdGJjJnTdw4rK8Lsklasu_t0R1mnqk5Uj-Y57ptQ.wWAApHlMGsz_Y-_f.x_qvL9EwVSm2PeIsX67ixAay35udiMSw0HoXk9B6mE8GP0LzwWJP-pHkP-dWYWW1MtGWvq1CYYxoZaU0mhaJpo9zPSfOjq0Yb26WylVn5FgcNiir38qtWYqobqsw65CXy7aGuIRDM5b6io8QQw5EWvhv29T6oyRPZpx5abdTEkCtSSGsA_I0DFU7mn3FV9fAhTgg3XcE_9smnIAau-rBXurqwYUJESGXpibg9l6qx3gpPeJ5di2be9R51JPcv8AtXwUpXjtwU3h8D1AGikyFmpNgDC-qi9msiaGsNHSFj1gAW5uNczMbWxbWO3r6gZ9C4KiUJdmnQuRiqqbrLvwYezhy15vpr4yGgFxL1qpzVm5bQ3ta4BYoeKLu4rz9iwXu2X1WmhAYewl50Cce1iguQwr-x3j6ExgYD_1WL7iyU805vaz3bXGRpx3PRGEmMEbZOy-CE_BuYNOO9ED_JkJtMOVXp_MOoFvHESg.gqcP4W4DSRGBoqYUfBIZUA";
        break;

    case "cimb": //auth token for cimb
        authToken = "mBzHeFN+mt0Olz8a0pq1wx0v5VREEpWFFmRRtPhBM+dCuFWh8pyYk8rsEO1AdFz4chHlz+1ME8bZwCfFgg0ed9dVeCgbIc9zmeZIig/9zKkQB9JQIap/aHtra47XY1SLHfmfjJH7DMJ+Y8tFwPrPzXPKd/jQV5QrChxlt2kfsLJgoDForqnR7zmCdpBpEOCLM94dcejJsb+hhUakNM7YzdG/e8+5LzS+MK0ZURXivDTxN1Kml3w8j8sJABeJu0YJhqt9jDssAmkmRP9LamyF/QEKHWnz8pz9p+1Q74vXYHSk0NejuoF9cOZ3k7kwGXO4RTgeCy+YAeA69X0/QIPyVQ==";
        break;
}

var disconnectChatOnClickToCallEnd = true; //Used to disconnect the chat if click to call is ended

var phoneNotificationCommands = [ //Commands used by phone app Notifications
    "UI_STATUS",
    "CONNECTIVITY_STATUS"
]

var __logConfigurations = {
    sdk: {
        enableConsoleLogging: true, //Enables/Disables SDK Browser Console Logging
        enableDebugConsoleLogs: true, //Enables/Disables SDK Browser Debug Console Logs
        enableLogMasking: false, //Enables/Disables SDK Log Masking
    }
}

var __webRTCConfigurations = {
    iceServers: [
        //{ urls: "turn:test.tetherficloud.com:3579?transport=udp", username: "tetherfi", credential: "nuwan" },
        { urls: "turn:proxy.lab.tetherfi.cloud:3478?transport=tcp", username: "tetherfi", credential: "tetherfi" }
        // { urls: "turn:3.233.143.234:3579", username: "tetherfi", credential: "tetherfi" }
    ],
    iceTransportPolicy: "all",
    mediaCapture: {
        audio: { enabled: true },
        video: { enabled: true }
    },
    network: {
        Bandwidth: {
            // The max kbps for a single audio track (integer between 6 - 64)
            SingleAudioTrackLimit: 8,
            // The max kbps for a single video track (integer between 128 - 2048)
            SingleVideoTrackLimit: 128,
            // The max kbps for all video and audio tracks (integer)
            CumelativeSessionLimit: 2048
        }
    },
    voiceActivity: {
        AudioLevelCheckEnabled: true,
        AudioVideoLevelThreshold: 0.31
    },
    // 0 - The call will be made normally and will be recorded if a MediaServer is present in between and configured accordingly.
    // 1 - The call will be made such that all video streams will flow as described in Normal mode (via MediaServer) and all audio streams will bypass any intermediate MediaServer even if such is present in between.
    // 2 - The call will be made such that all audio streams will flow as described in Normal mode (via MediaServer) and all video streams will bypass any intermediate MediaServer even if such is present in between.
    // 3 - The call will bypass any intermediate MediaServer setup all-together and will connect P2P straight to the browser at other end.
    audioVideoConnectionMode: 0,
    tryToMergeSingleTrackStreamsFromDifferentSources: false,
}

var __defaultSettings = {
    showSettingsPage: true, //Shows/Hides the Settings Section
    general: {
        showConsole: false, //Shows/Hides the console that appears (to the right) in the testapp
    },
    messaging: {
        showNewMessagingUI: true, //Shows/Hides the new Messaging UI (CURRENTLY COMPATIBLE WITH INTERACTION AND ENTERPRISE BASED MESSAGING PLATFORMS). This UI will be shown by default if the __defaultMessagingType is enterprise/interaction regardless of this setting.
        showSimulateNotificationsSection: false, //Shows/Hides the 'Simulate Phone Notifications' section in the Messaging Section
    },
    groups: {
        enabled: false, //Shows/Hides the Groups Section
    },
    av: {
        showAVConsole: false, //Shows/Hides the A/V console in the Audio/Video Section
    },
}

var __defaultMessagingType = "interaction"; //Selects the default messaging type on the login/main page (Supported values are enterprise/interaction/cloud)

var __customerInformation = { //This contains the customer info which will be sent as part of the login or session start request
    name: "Livechat2 TestApp User", 
    address: "PC",
    pIntent: "dice", //Applicable for enterprise setup (if chat is to be routed to a specific intent)
}