var bStartWithoutLogin = true;
var __version = "1.21.3.6";
var __serverConnectionStatus;
var __userId = "";
var __requestId = "";
var __channel = ""; //Identify the channel
var isVideoCall = null; //True if the video tab is selected, false if audio is selected, and null if neither audio nor video is selected
var __availableInputDevices = { audio: [], video: [] }; // Contains the list of audio & video input devices
var __selectedAudioInputDevice = null; //Selected audio input device info
var __selectedVideoInputDevice = null; //Selected video input device info
var notificationTypes = {
    info: "info",
    success: "success",
    error: "error",
    warning: "warning"
}
var __interactionManager = {}; //Used for managing interactions