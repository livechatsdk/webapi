function generateTimeFromUnixTimestamp(unixTimestamp) {
    try {
        if (unixTimestamp != null) {
            var date = new Date(unixTimestamp);
            // Hours part from the timestamp
            var hours = date.getHours();
            // Minutes part from the timestamp
            var minutes = "0" + date.getMinutes();
            // Seconds part from the timestamp
            var seconds = "0" + date.getSeconds();

            // Will display time in 10:30:23 format
            return `${hours}:${minutes.substr(-2)}:${seconds.substr(-2)}`;
        }
        else
            return ``;
    } catch (error) {
        logError(`Error in generateTimeFromUnixTimestamp`, error);
    }
}