window.addEventListener("load", init, false);

function init() {
    logInfo(writeToScreen(`Testapp Version: ${__version}`));

    userList.forEach(element => {
        var obj = element;
        var emailString = "'" + obj.email.toString() + "'";
        var passwordString = "'" + obj.password.toString() + "'";
        var onclick = 'AutoFillDetails(' + emailString + ',' + passwordString + ')'
        var ui = "<tr onclick=" + onclick + "><th scope='row'>" + obj.email + "</th><td>" + obj.password + "</td></tr >"
        var htmlObject = $(ui);
        document.getElementById('user-list-grid-body').append(htmlObject[ 0 ]);
    });

    output = document.getElementById("output");

    if (livechatapi.open() != false) {
        livechatapi.getUserInteractionList();
        ChangePageContent(true);
        writeToScreen("Open Success");
        writeToScreen("User Id: " + livechatapi.livechat.getUserId());
        writeToScreen("Session Id: " + livechatapi.livechat.getSessionID());
        ShowNotification(notificationTypes.success, "Login Successful");
    }

    InitializeDefaultSettings();

    if (__defaultMessagingType != null && (__defaultMessagingType === "enterprise" || __defaultMessagingType === "interaction")) {
        //If the messaging type in enterprise or interaction we show the new messaging ui by default
        document.getElementById('new-messaging-ui-toggle').classList.add(`active`);

        document.getElementById('messaging-primary').classList.add('hidden');
        document.getElementById('messaging-secondary').classList.remove('hidden');
    }

    OnSelectMessagingSection(__defaultMessagingType);

    if (phoneNotificationCommands != null && Array.isArray(phoneNotificationCommands) && phoneNotificationCommands.length > 0) {
        phoneNotificationCommands.forEach(command => {
            var anchor = document.createElement("a");
            anchor.classList.add("dropdown-item");
            anchor.href = "#";
            anchor.innerText = command;
            document.getElementById("phone-notification-command-button-dropdown").appendChild(anchor);
        });
    }

    $("#phone-notification-command-button-dropdown a").click(function () {
        $(this).parents(".dropdown").find('.btn').text($(this).text());
    });

    try {
        //Initialize callsdk
        initCallSdk();

        //Check if WebRTC is supported on the browser
        callSdkGlobal.call.CheckDeviceSupport().then(function (deviceInfo) {
            if (deviceInfo.isWebRTCSupported === false)
                ShowNotification(notificationTypes.error, "WebRTC is not supported on this browser");
            else
                //If webrtc is supported call getUserMedia once so that browser will request for accessing camera/mic (especially on mobile phones), and then get the list of devices
                window.navigator.mediaDevices.getUserMedia({ audio: true, video: true })
                    .then(function (stream) {
                        stream.getTracks().forEach(function (track) {
                            track.stop();
                        });

                        //Get the list of all input devices and store them in a variable
                        callSdkGlobal.call.CheckDeviceSupport().then(function (deviceInfo) {
                            deviceInfo.MediaDevices.forEach(function (device) {
                                if (device.kind === "audioinput")
                                    __availableInputDevices.audio.push(device);
                                else if (device.kind === "videoinput")
                                    __availableInputDevices.video.push(device);
                            });

                            ShowAudioVideoInputDevices();
                        });
                    })
                    .catch(error => logError(`Error in init - getUserMedia`, error));
        });
    } catch (error) {
        logError(`Error in init`, error);
    }

    window.onresize = resizeMessagingArea;
}

function AutoFillDetails(emailId, password) {
    try {
        document.getElementById('email').value = emailId;
        document.getElementById('password').value = password;
    } catch (e) {

    }
}

function OnSelectAudioVideo(isVideo) {
    $("#callType-dropdown-menu > a").removeClass('active');

    for (let child of document.getElementById("callType-dropdown-menu").children) {
        if (child.innerHTML.toLowerCase() === "video" && isVideo === true)
            child.classList.add("active");
        else if (child.innerHTML.toLowerCase() === "audio" && isVideo === false)
            child.classList.add("active");
    }

    document.getElementById("callType").innerHTML = isVideo === true ? "Video" : "Audio";

    isVideoCall = isVideo;
}

function GetEWTStatus() {
    livechatapi.getEWTStatus(__requestId);
}

function ShowEWTStatusButton(isShow) {
    if (isShow) {
        document.getElementById("get-ewt-status-button").classList.remove("hidden");
    }
    else {
        document.getElementById("get-ewt-status-button").classList.add("hidden");
    }
}

function ShowAudioVideoInputDevices() {
    try {
        var count = 0;
        __availableInputDevices.audio.forEach(function (audioDevice) {
            var anchorDivElement = document.createElement("a");
            anchorDivElement.href = "#";
            anchorDivElement.classList.add("dropdown-item");
            anchorDivElement.innerHTML = `${++count}. ${audioDevice.label}`;
            anchorDivElement.onclick = () => { OnInputDeviceSelected("audio", audioDevice.deviceId, anchorDivElement) }

            document.getElementById("audio-input-devices-dropdown-menu").appendChild(anchorDivElement);
        });

        count = 0;
        __availableInputDevices.video.forEach(function (videoDevice) {
            var anchorDivElement = document.createElement("a");
            anchorDivElement.href = "#";
            anchorDivElement.classList.add("dropdown-item");
            anchorDivElement.innerHTML = `${++count}. ${videoDevice.label}`;
            anchorDivElement.onclick = () => { OnInputDeviceSelected("video", videoDevice.deviceId, anchorDivElement) }

            document.getElementById("video-input-devices-dropdown-menu").appendChild(anchorDivElement);
        });
    } catch (error) {
        logError(`Error in ShowAudioVideoInputDevices`, error);
    }
}

function OnInputDeviceSelected(type, deviceId, element) {
    try {
        //Mantain an internal state to know if the audio/video selected input device has changed
        var isSelectedAudioDeviceChanged = false;
        var isSelectedVideoDeviceChanged = false;

        if (type === "audio") {
            //Iterate existing audio input devices
            __availableInputDevices.audio.some(function (audioDevice) {
                //If a match is found
                if (audioDevice.deviceId === deviceId) {
                    //Only assign the newly selected input device, if there was no device selected previously or if the previously selected input device is different from the newly selected input device
                    if (__selectedAudioInputDevice == null || (__selectedAudioInputDevice != null && __selectedAudioInputDevice.deviceId != audioDevice.deviceId)) {
                        __selectedAudioInputDevice = audioDevice;
                        isSelectedAudioDeviceChanged = true;
                    }

                    $("#audio-input-devices-dropdown-menu > a").removeClass('active');
                    element.classList.add('active');

                    return true;
                }
                else
                    return false;
            });
        }
        else if (type === "video") {
            //Iterate existing video input devices
            __availableInputDevices.video.some(function (videoDevice) {
                //If a match is found
                if (videoDevice.deviceId === deviceId) {
                    //Only assign the newly selected input device, if there was no device selected previously or if the previously selected input device is different from the newly selected input device
                    if (__selectedVideoInputDevice == null || (__selectedVideoInputDevice != null && __selectedVideoInputDevice.deviceId != videoDevice.deviceId)) {
                        __selectedVideoInputDevice = videoDevice;
                        isSelectedVideoDeviceChanged = true;
                    }

                    $("#video-input-devices-dropdown-menu > a").removeClass('active');
                    element.classList.add('active');

                    return true;
                }
                else
                    return false;
            });
        }

        //If the audio/video device has changed, then we trigger the setInputDevice method
        if (isSelectedAudioDeviceChanged === true || isSelectedVideoDeviceChanged === true)
            setInputDevice().then(function (status) {
                if (status === true) {
                    if (isSelectedAudioDeviceChanged === true && isSelectedVideoDeviceChanged === true)
                        ShowNotification(notificationTypes.success, "Audio & Video sources changed");
                    else if (isSelectedAudioDeviceChanged === false && isSelectedVideoDeviceChanged === true)
                        ShowNotification(notificationTypes.success, "Video source changed");
                    else if (isSelectedAudioDeviceChanged === true && isSelectedVideoDeviceChanged === false)
                        ShowNotification(notificationTypes.success, "Audio source changed");
                }
                else {
                    if (isSelectedAudioDeviceChanged === true && isSelectedVideoDeviceChanged === true)
                        ShowNotification(notificationTypes.error, "Audio & Video sources could not be changed");
                    else if (isSelectedAudioDeviceChanged === false && isSelectedVideoDeviceChanged === true)
                        ShowNotification(notificationTypes.error, "Video source could not be changed");
                    else if (isSelectedAudioDeviceChanged === true && isSelectedVideoDeviceChanged === false)
                        ShowNotification(notificationTypes.error, "Audio source could not be changed");
                }
            });
    } catch (error) {
        logError(`Error in OnInputDeviceSelected`, error);
    }
}