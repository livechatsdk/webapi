function loginAndConnect() {
    bStartWithoutLogin = false;
    try {
        event.preventDefault();

        email = document.getElementById('email').value;
        var password = document.getElementById('password').value;

        if (email == "" || password == "") {
            return;
        }

        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementById('user-login-form');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });

        if (useTenancyBasedLogin) {
            if (loginAuthURL === undefined || loginAuthURL === "") {
                ShowNotification(notificationTypes.error, "loginAuthURL has not been configured");
                return;
            }

            livechatapi.loginV2(email, password, onSuccess, loginAuthURL);
        }
        else
            livechatapi.login(email, password, onSuccess);
    } catch (error) {
        logError(`Error in loginAndConnect`, error);
    }
}

function end(reason) {
    writeToScreen("SENT: CHAT END ");
    try {
        if (callSdkGlobal != null && callSdkGlobal.call != null) {
            callSdkGlobal.call.StopScreensharing();
            callSdkGlobal.call.EndAvCall();
        }
    } catch (error) {
        logError(`Error in end - callSdkGlobal`, error);
    }
    if (reason != null)
        livechatapi.end(reason);
    else
        livechatapi.end('client side end.');

    document.getElementById('msg-userId').value = "";
    document.getElementById('toId').value = "";

    clearAllInteractions();

    ChangePageContent(false);
}

function logout(reason) {
    livechatapi.logout(reason, onLogout);
}

function onLogout(status) {
    logInfo(`status: ${status}`);

    if (status === true)
        end();
}

function onSuccess(status, auth, info) {

    logInfo(`status: ${status}`);
    logInfo(`token: ${auth}`);
    logInfo(`token Info : ${info}`);

    writeToScreen("email :" + email);
    writeToScreen("User Id:" + livechatapi.livechat.getUserId());

    if (status) {
        document.getElementById('email').value = '';
        document.getElementById('password').value = '';
        openChannel(true, 'chat');
        ShowNotification(notificationTypes.success, "Login Successful");
    }
    else {
        alert("Login Failed");
        ShowNotification(notificationTypes.error, "Login Failed");
    }
}

function openChannel(bNormalLogin, channel) {
    __channel = channel;

    if (bNormalLogin === false) {
        __userId = GenerateRandomId(64);
        //let userId = Math.floor(Math.random() * 1000000).toString();
        writeToScreen("User ID: " + __userId);

        let attributes = {};
        if (sessionId != null && sessionId != undefined) {
            attributes[ "sessionId" ] = sessionId;
        }
        if (authType != null && authType != undefined) {
            attributes[ "authType" ] = authType;
        }
        livechatapi.setUserAuth(__userId, authToken, attributes);
    }

    livechatapi.start(__customerInformation);
    writeToScreen("SENT: START MESSAGE");
}

function overrideSession() {
    livechatapi.start(__customerInformation, true);
    writeToScreen("SENT: START FETCH MESSAGE");

    $('#sessionModal').modal('hide');
}

function startAV() {
    document.getElementById("start-call-button").disabled = true;
    openChannel(false, 'video');
    // livechatapi.init();
    // livechatapi.start(__customerInformation);
}

function findUser() {
    var customerName = document.getElementById("customerName").value;
    var customerId = document.getElementById("customerId").value;
    var language = "English";
    var pageId = "Accounts";
    var mobileAuthFlag = true;
    var mobileNumber = "919988774321";

    var __intent = document.getElementById("intentId").value;
    var __channel = document.getElementById("channel").value;

    let customerInfo = "";
    __requestId = Math.floor(Math.random() * 1000000).toString()

    if (__disableAuthTokenValidation != null && __disableAuthTokenValidation) {
        customerInfo = JSON.stringify({ customerId, customerName, mobileNumber, pageId, mobileAuthFlag });
    }
    else {
        customerInfo = "";
    }

    livechatapi.findUser(__requestId, customerInfo, __intent, __channel, language);

    $('#findUserModal').modal('hide');
    document.getElementById("btnFindUser").disabled = true;
    writeToScreen("Finding User with Request Id: " + __requestId);
}

function NavbarSwitch(navbar) {
    try {
        $("#testapp-navs > li").removeClass("active");
        $("#" + navbar + "-nav-item").addClass("active");
        $("#testapp-navs-content > div").addClass("hidden");
        $("#" + navbar + "-content").removeClass("hidden");
    } catch (error) {
        logError(`Error in NavbarSwitch`, error);
    }
}

function ChangePageContent(isStart) {
    try {
        if (isStart) {
            $("#login-page").addClass("hidden");
            $("#content-page").removeClass("hidden");
            resizeMessagingArea();
        }
        else {
            $("#login-page").removeClass("hidden");
            $("#content-page").addClass("hidden");
            document.getElementById("start-call-button").disabled = false;
        }
    } catch (error) {
        logError(`Error in ChangePageContent`, error);
    }
}

function GenerateRandomId(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function OnSelectMessagingSection(messagingType) {
    try {
        $("#messagingType-dropdown-menu > a").removeClass('active');
        document.getElementById(`messagingType-dropdown-${messagingType}`).classList.add("active");
        document.getElementById("messagingType").innerHTML = document.getElementById(`messagingType-dropdown-${messagingType}`).innerHTML;

        if (messagingType != null && messagingType != "")
            if (messagingType === "enterprise") {
                document.getElementById(`cloud-login-credentials-table`).classList.add(`hidden`);
                document.getElementById(`cloud-login-form`).classList.add(`hidden`);
                document.getElementById(`cloud-loging-button`).classList.add(`hidden`);
                document.getElementById(`start-button`).classList.remove(`hidden`);
                document.getElementById(`start-call-button`).classList.add(`hidden`);
            }
            else if (messagingType === "cloud") {
                document.getElementById(`cloud-login-credentials-table`).classList.remove(`hidden`);
                document.getElementById(`cloud-login-form`).classList.remove(`hidden`);
                document.getElementById(`cloud-loging-button`).classList.remove(`hidden`);
                document.getElementById(`start-button`).classList.add(`hidden`);
                document.getElementById(`start-call-button`).classList.add(`hidden`);
            }
            else if (messagingType === "interaction") {
                document.getElementById(`cloud-login-credentials-table`).classList.add(`hidden`);
                document.getElementById(`cloud-login-form`).classList.add(`hidden`);
                document.getElementById(`cloud-loging-button`).classList.add(`hidden`);
                document.getElementById(`start-button`).classList.remove(`hidden`);
                document.getElementById(`start-call-button`).classList.remove(`hidden`);
            }
            else
                logError(`Unknown Messaging Type: ${messagingType}`);
    } catch (error) {
        logError(`Error in OnSelectMessagingSection`, error);
    }
}