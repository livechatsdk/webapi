function writeToScreen(message, extId) {
    document.getElementById("output").style.display = 'block';

    document.getElementById("output").innerHTML += `<div><p class="card-text" style="margin-bottom: 8px;">${message}</p></div>`

    document.getElementById("output").scrollTop = document.getElementById("output").scrollHeight;
    return message;
}

function ShowNotification(notificationType, message) {
    switch (notificationType) {
        case notificationTypes.info:
            PNotify.info({
                text: message
            });
            break;
        case notificationTypes.success:
            PNotify.success({
                text: message
            });
            break;
        case notificationTypes.error:
            PNotify.error({
                text: message
            });
            break;
        case notificationTypes.warning:
            PNotify.notice({
                text: message
            });
            break;
    }
}

PNotify.defaultModules.set(PNotifyMobile, {});
PNotify.defaultModules.set(PNotifyBootstrap4, {});
PNotify.defaultModules.set(PNotifyFontAwesome5Fix, {});
PNotify.defaultModules.set(PNotifyFontAwesome5, {});
PNotify.defaults.delay = 2000;
PNotify.defaults.closer = true;
PNotify.defaultStack.dir2 = "right";

function showTyping() {
    var typingElement = document.getElementById("typing");

    if (typeof (typingElement) == 'undefined' || typingElement == null) {
        document.getElementById("output").innerHTML += "<div id='typing' class='ticontainer'> <div class='tiblock'> <div class='tidot'></div> <div class='tidot'></div> <div class='tidot'></div></div> </div>";
        document.getElementById("output").scrollTop = document.getElementById("output").scrollHeight;
    }
}

function hideTyping() {
    var typingElement = document.getElementById("typing");
    if (typingElement != null)
        typingElement.parentNode.removeChild(typingElement);
}

var __signalStrengthNotificationTimer = null;

function onSignalStrengthChange(strength) {
    try {
        if (strength <= 1) {
            //Signal Strength is bad
            ShowSignalStrengthNotification("Signal Strength (" + strength + ") is bad");
        }
        else if (strength > 1 && strength < 2) {
            //Signal Strength is Poor
            ShowSignalStrengthNotification("Signal Strength (" + strength + ") is Poor");
        }
        else if (strength === 2) {
            //Signal Strength is Poor
            ShowSignalStrengthNotification("Signal Strength (" + strength + ") is Poor");
        }
        else if (strength > 2 && strength < 3) {
            //Signal Strength is Fair
            ShowSignalStrengthNotification("Signal Strength (" + strength + ") is Fair");
        }
        else if (strength === 3) {
            //Signal Strength is Fair
        }
        else if (strength > 3 && strength < 4) {
            //Signal Strength is Fair
        }
        else if (strength === 4) {
            //Signal Strength is Good  
        }
        else if (strength > 4 && strength < 5) {
            //Signal Strength is Good
        }
        else if (strength === 5) {
            //Signal Strength is Excellent
        }
    } catch (error) {
        logError(`Error in onSignalStrengthChange`, error);
    }
}

function ShowSignalStrengthNotification(message) {
    try {
        if (__signalStrengthNotificationTimer === null && videoSignalStrengthMessages.enabled) {
            ShowNotification(notificationTypes.error, message);
            __signalStrengthNotificationTimer = setTimeout(function () {
                clearTimeout(__signalStrengthNotificationTimer);
                __signalStrengthNotificationTimer = null;
            }, videoSignalStrengthMessages.notificationInterval);
        }
    } catch (error) {
        logError(`Error in ShowSignalStrengthNotification`, error);
    }
}

function logInfo(message) {
    let dateTime = this.generateLogDateTime();

    if (message != null)
        console.info(`[${dateTime}] - ${message}`);
}

function logDebug(message) {
    let dateTime = this.generateLogDateTime();

    if (message != null)
        console.debug(`[${dateTime}] - ${message}`);
}

function logWarn(message) {
    let dateTime = this.generateLogDateTime();

    if (message != null)
        console.warn(`[${dateTime}] - ${message}`);
}

function logError(message, errorObject = null) {
    let dateTime = this.generateLogDateTime();

    if (message != null) {
        if (errorObject != null) {
            if (typeof errorObject === 'object')
                console.error(`[${dateTime}] - ${message}${errorObject.name != null ? `\nName: ${errorObject.name}` : ''}${errorObject.description != null ? `\nDescription: ${errorObject.description}` : ''}${errorObject.message != null ? `\nMessage: ${errorObject.message}` : ''}${errorObject.stack != null ? `\nStack: ${errorObject.stack}` : ''}`);
            else
                console.error(`[${dateTime}] - ${message}\nError Details: ${JSON.stringify(errorObject)}`)
        }
        else
            console.error(`[${dateTime}] - ${message}`);
    }
}

function generateLogDateTime() {
    let date = new Date();
    return (date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds() + ":" + date.getMilliseconds()).toString();
}